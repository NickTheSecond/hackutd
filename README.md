# Drive, Don't Fly

Our group got the idea for this project from the prompts focusing on social benefit. We felt that the environment was an important topic that we could address with a web application. We considered the environmental impacts of transportation, and ultimately decided that we wanted to create a service that makes traveling long distances cheaper and more environmentally friendly.