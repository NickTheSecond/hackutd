from pymongo import MongoClient
import pymongo
import math
import requests
import ast

def getalldrivers(client):
    drivers = client.drivers.preferences
    returns = []
    for d in drivers.find():
        d.pop('_id')
        returns.append(d)
    return returns

def geteligibledrivers(point, alldrivers):
    drivers = set()
    for driver in alldrivers:
        latitude_diff = point[1] - driver['home']['geometry']['y']
        longitude_diff = point[0] - driver['home']['geometry']['x']
        latitude_miles = 69 * latitude_diff
        longitude_miles = math.cos(point[1] * math.pi / 180) * 69.172 * longitude_diff
        if math.sqrt(latitude_miles**2 + longitude_miles**2) <= float(driver['radius']):
            drivers.add(str(driver))

    return drivers

def buildroute(client, ride):
    total_path = []
    for direction in ride['directions']:
        for path in direction['geometry']['paths']:
            total_path += path
    
    driverpath = []
    alldrivers = getalldrivers(client)
    validDrivers = geteligibledrivers(total_path[0], alldrivers)
    transfers = []
    print(ride)
    transfers.append({'place': {'x': ride['start']['geometry']['x'], 'y': ride['start']['geometry']['y']}, 'spatialReference': ride['start']['geometry']['spatialReference']})
    #transfers[0]['place'] = transfers[0]['paths'][0]
    #del transfers[0]['paths']
    for point in total_path:
        nextdrivers = validDrivers & geteligibledrivers(point, alldrivers)
        if len(nextdrivers) == 0:
            vd = validDrivers.pop()
            print(vd)
            driverpath.append(eval(vd))
            transfers.append(getNearest(point))
            nextdrivers = geteligibledrivers(point, alldrivers)
        validDrivers = nextdrivers
    if len(validDrivers) > 0:
        vd = validDrivers.pop()
        print(vd)
        driverpath.append(eval(vd))
    #transfers += [ride['directions'][0]['geometry']]
    #transfers[-1]['place'] = transfers[-1]['paths'][0]
    #del transfers[-1]['paths']
    transfers.append({'place': {'x': ride['stop']['geometry']['x'], 'y': ride['stop']['geometry']['y']}, 'spatialReference': ride['stop']['geometry']['spatialReference']})

    return [driverpath, transfers]
    

def getNearest(point):
    url = f"https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?f=json&category=gas%20station&location={point[0]},{point[1]}&outFields=Place_addr,PlaceName&maxLocations=5"
    result = requests.get(url).json()
    result['place'] = result['candidates'][0]['location']
    result.pop('candidates')
    return result



def hello_world(request):
	
    if request.method == 'OPTIONS':
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }
        return ('', 204, headers)
    else:
        request_json = request.json
        client = MongoClient('mongodb+srv://jordan:V5Kb2anXeIfyUI4y@hackutd-qyo9r.gcp.mongodb.net/test?retryWrites=true&w=majority')
        db = client.riders
        people = db.rides
        fullRide = db.fullrides
        print(request_json)
        people.insert_one(request_json)
        drivers, transfers = buildroute(client, request_json)
        fullRide.insert_one({'drivers': drivers, 'transfers': transfers, 'name': request_json['name']})
        
        todays_ride = client.todays_ride
        tr = todays_ride.tr
        tr.update_one({}, {'$set': {'name': request_json['name']}}, upsert=True)
        return('added', 200, {'Access-Control-Allow-Origin':'*'})
