from pymongo import MongoClient
import pymongo
import math
import requests
import json

def hello_world(request):
	
    if request.method == 'OPTIONS':
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }
        return ('', 204, headers)
    else:
        request_json = request.json
        client = MongoClient('mongodb+srv://jordan:V5Kb2anXeIfyUI4y@hackutd-qyo9r.gcp.mongodb.net/test?retryWrites=true&w=majority')
        db = client.riders
        fullrides = db.fullrides
        print(request_json)
        tr = client.todays_ride.tr
        trname = tr.find_one()['name']
        ride = fullrides.find_one({'name': trname})
        driver_i = None
        for i in range(len(ride['drivers'])):
            if request_json['name'] == ride['drivers'][i]['name']:
                driver_i = i
                break
        else:
            return('', 418, {'Access-Control-Allow-Origin':'*'})
        
        result = {'transfers': [ride['transfers'][i], ride['transfers'][i+1]]}
        return(json.dumps(result), 200, {'Access-Control-Allow-Origin':'*'})
