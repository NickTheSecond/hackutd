import json
import requests
import datetime
#from amadeus import Client, ResponseError
from amadeus import *
from requests.auth import HTTPBasicAuth

def flightFootprint(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    request_json = request.get_json()
    origin = request_json['origin']
    dest = request_json['destination']
    date = request_json['date']

# args:
#origin = 'DFW'
#dest = 'DEN'
#date = '2019-11-12'

    climateNeutralKey = "f1e8ec80c46636a8b1021a5e"
    amadeus = Client()


    data = amadeus.get('/v1/shopping/flight-offers', origin='DFW', destination='DEN', departureDate=date)
    #data = amadeus.shopping.flight_offers(origin='DFW', destination='DEN')

    lowestPrice = -1
    lowestPriceID = ''
    for option in data.result['data']:
        price = float(option['offerItems'][0]['price']['total'])
        # print('Found price ${}.'.format(price))
        flightID = option['id']
        if price < lowestPrice or lowestPrice == -1:
            lowestPrice = price
            lowestPriceID = flightID

    print("Lowest flight ID: {}, price: ${}".format(lowestPriceID, lowestPrice))

    par = {'segments[0][origin]' : origin, 'segments[0][destination]' : dest, 'cabin_class' : 'economy', 'currencies' : 'USD'}
    # par = [{'origin': 'DFW', 'destination': 'DEN'}]
    cnJson = json.loads(requests.get('http://api.goclimateneutral.org/v1/flight_footprint', params=par, auth=HTTPBasicAuth(climateNeutralKey, '')).text)
    footprint = cnJson['footprint'] 

    # cnData2 = requests./v1/flight_footprint?segments[0][origin]=ARN&segments[0][destination]=BCN&segments[1][origin]=BCN&segments[1][destination]=ARN&cabin_class=economy&currencies[]=SEK HTTP/2
    #print("done")

    return json.dumps({"footprint" : footprint, "cost" : lowestPrice})
