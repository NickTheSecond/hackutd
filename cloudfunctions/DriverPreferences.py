from pymongo import MongoClient
import pymongo
def hello_world(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)
    else:
        request_json = request.json
        client = MongoClient('mongodb+srv://jordan:V5Kb2anXeIfyUI4y@hackutd-qyo9r.gcp.mongodb.net/test?retryWrites=true&w=majority')
        db = client.drivers
        people = db.preferences
        people.insert_one(request_json)
        return('added',200, {'Access-Control-Allow-Origin':'*'}) 
        

